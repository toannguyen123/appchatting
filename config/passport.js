// config/passport.js
var LocalStrategy = require('passport-local').Strategy;
var User = require('../app/models/user');

module.exports = (passport) => {

    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser((id, done) => {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
        function (req, email, password, done) { // callback with email and password from our form
            console.log(req.body.username);
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            User.findOne({ 'local.username': req.body.username }, function (err, user) {
                // if there are any errors, return the error before anything else
                if (err)
                    return done(err);

                // if no user is found, return the message
                // OR if the user is found but the password is wrong
                if (!user || !user.validPassword(password)) {
                    console.log('wrong user ');
                    return done(null, false, req.flash('loginMessage', 'Please check your username and password.')); // create the loginMessage and save it to session as flashdata
                }

                User.findOneAndUpdate({ 'local.username': req.body.username }, { $set: { 'local.isonline': true } }, (err, result) => {
                    if (err)
                        return done(err);
                })

                // all is well, return successful user
                return done(null, user);
            });

        }));

    passport.use('local-signup', new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        email: 'email',
        passReqToCallback: true
    },
        function (req, email, password, done) {
            process.nextTick(function () {
                User.findOne({ 'local.email': req.body.email }, function (err, user) {
                    if (err)
                        return done(err);
                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'Email existed .'));
                    } else {
                        User.findOne({ 'local.username': req.body.username }, function (err, user) {
                            if (err)
                                return done(err);
                            if (user) {
                                return done(null, false, req.flash('signupMessage', 'Username  existed  .'));
                            } else {
                                var newUser = new User();
                                newUser.local.email = req.body.email;
                                newUser.local.username = req.body.username;
                                newUser.local.password = newUser.generateHash(password);
                                newUser.local.isonline = false;
                                newUser.save(function (err) {
                                    if (err)
                                        throw err;
                                    return done(null, newUser);
                                });
                            }

                        });

                    }

                });

            });

        }));
};