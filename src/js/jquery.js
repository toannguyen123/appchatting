$(function () {
    $('img').click(function () {
        if(!$(this).hasClass('icon-active') && !$(this).hasClass('light-box')){
            $('img').css('border-bottom','none');
        }
    })
    $('.nav-item').click(function(){
        $('.nav-item img').css('display','none');
        $(this).find('img').css('display','block');
    })
});

const { range } = rxjs;
const { map, filter } = rxjs.operators;

range(1, 200).pipe(
  filter(x => x % 2 === 1),
  map(x=>x+x)
).subscribe(x => console.log(x));