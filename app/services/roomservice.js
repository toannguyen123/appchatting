class RoomService {

  constructor() {
    this.roomModel = require('../models/room');
  }
  async createroom(room) {
    const createdroom = new this.roomModel(room);
    const takeroom = await createdroom.save();
    return takeroom;
  }

  async findAllRoom() {
    return await this.roomModel.find({ 'typeroom': 'channel' }).exec();
  }

  async findRoomOnTop() {
    return await this.roomModel.find({ 'typeroom': 'channel' }).sort({
      date: -1
    })
      .limit(1).exec();
  }

  async findOneRoomById(idroom) {
    return await this.roomModel.findOne({ 'idroom': idroom }).exec();
  }

  async findOneRoom(roomname) {
    return await this.roomModel.findOne({ 'roomname': roomname }).exec();
  }

  async updateMessage(data) {
    await this.roomModel.findOneAndUpdate({ 'idroom': data.idroom },
      { $push: { messages: { iduser: data.messages.iduser, username: data.messages.username, text: data.messages.text } } }).exec();

  }

  async getMessage(idroom) {
    return await this.roomModel.findOne({ 'idroom': idroom }).exec();
  }

  async findOneByRoomName(roomname) {
    return await this.roomModel.findOne({ 'roomname': roomname }).exec();
  }

  async findOneByIdRoom(id) {
    return await this.roomModel.findOne({ 'idroom': id }).exec();
  }

  async updateRoom(id, room) {
    return await this.roomModel.findOneAndUpdate({ '_id': id }, room).exec();
  }

  async searchRoom(textSearch) {
    return await this.roomModel.find(
      {
        $and: [{ 'typeroom': 'channel' },
        { $or: [{ 'title': { '$regex': textSearch } }, { 'messages': { $in: [{ '$regex': textSearch }] } }] }
        ]
      }).exec();
  }

  async deleteRoom(id) {
    return await this.roomModel.findOneAndRemove({ '_id': id }).exec();
  }

}

module.exports = new RoomService();