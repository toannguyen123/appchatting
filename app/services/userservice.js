class UsersService {

  constructor() {
    this.userModel = require('../models/user');
  }
  async createUser(user) {
    const createdUser = new this.userModel(user);
    const takeUser = await createdUser.save();
    return takeUser;
  }

  async findAllUsers() {
    return await this.userModel.find({}, function (err, users) {
      if (err) throw err;
      console.log(users);
      return users;
    })
  }

  async searchUser(textSearch) {
    return await this.userModel.find({
      $or: [
        { 'local.username': { '$regex': textSearch } },
        { 'local.email': { '$regex': textSearch } }
      ]

    }, function (err, users) {
      if (err) throw err;
      return users;
    })
  }
  async findOneUsers(username) {
    return await this.userModel.findOne({ 'username': username }).exec();
  }

  async findOneByUsername(username) {
    return await this.userModel.findOne({ 'username': username }).exec();
  }

  async findOneById(id) {
    return await this.userModel.findOne({ '_id': id }).exec();
  }

  async updateUser(id, user) {
    return await this.userModel.findOneAndUpdate({ '_id': id }, user).exec();
  }

  async deleteUser(id) {
    return await this.userModel.findOneAndRemove({ '_id': id }).exec();
  }

}

module.exports = new UsersService();