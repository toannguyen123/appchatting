var mongoose = require('mongoose');


var roomSchema = mongoose.Schema({
    title: String,
    idroom: String,
    connections: String,
    typeroom: String,
    messages: [ { iduser: String,username:String, text: String } ],
    date: { type: Date, default: Date.now }
});



module.exports = mongoose.model('Room', roomSchema);