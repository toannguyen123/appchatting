// app/routes.js
var UsersService = require('./services/userservice');
var RoomService = require('./services/roomservice');
const uuidv1 = require('uuid/v1');

module.exports = (app, passport) => {
    app.get('/', isLoggedIn, async (req, res) => {
        const [listUser, listRoom,roomNew] = await Promise.all([
           UsersService.findAllUsers(),
           RoomService.findAllRoom(),
           RoomService.findRoomOnTop(),
        ])

        res.render('index.ejs', {
            listUser: listUser,
            user: req.user,
            listRoom: listRoom,
            roomNew:roomNew[0]
        });
    });

    app.get('/account/login', (req, res) => {
        res.render('account/login.ejs', { message: req.flash('loginMessage') });
    });

    app.post('/account/login', passport.authenticate('local-login', {
        successRedirect: '/account/profile',
        failureRedirect: '/account/login',
        failureFlash: true
    }));

    app.get('/account/register', (req, res) => {
        res.render('account/register.ejs', { message: req.flash('signupMessage') });
    });

    app.post('/account/register', passport.authenticate('local-signup', {
        successRedirect: '/account/profile',
        failureRedirect: '/account/register',
        failureFlash: true
    }));

    app.get('/account/profile', isLoggedIn, (req, res) => {
        var listUser = UsersService.findAllUsers();
        listUser.then(values => {
            res.render('account/profile.ejs', {
                user: req.user,
                listUser: values
            });
        });
    });

    app.get('/account/logout', (req, res) => {
        req.logout();
        res.redirect('/');
    });
    //User 
    app.get('/user/getUser', isLoggedIn, (req, res) => {
        var user = UsersService.findOneById(req.query.idFriendUser);
        user.then(values => {
            return res.status(200).json(values);
        });
    });
    app.get('/user/getMessage', isLoggedIn, (req, res) => {
        let roomId = req.query.idCurrentUser + "-" + req.query.idFriendUser;
        let roomIdNew = req.query.idFriendUser + "-" + req.query.idCurrentUser;
        var roomMessage = RoomService.getMessage(roomId);
        roomMessage.then(values => {
            if (values == null) {
                var roomMessageNew = RoomService.getMessage(roomIdNew);
                roomMessageNew.then(values => {
                    return res.status(200).json(values);
                });
            }
            else {
                return res.status(200).json(values);
            }

        });
    });
    //Api for room
    app.get('/room/addRoom', (req, res) => {
        let data = {
            idroom: uuidv1(),
            title: req.query.nameRoom,
            typeroom: 'channel'
        };
        RoomService.createroom(data).then(values => {
            return res.status(200).json(values);
        });
    });
    app.get('/room/getMessage', isLoggedIn, (req, res) => {
        var roomMessage = RoomService.getMessage(req.query.idChannel);
        roomMessage.then(values => {
            return res.status(200).json(values);
        });
    });
    app.get('/room/getAllRoom', isLoggedIn, (req, res) => {
        RoomService.findAllRoom().then(values => {
            return res.status(200).json(values);
        });
    });
    //
    app.get('/search/searchUserOrMessage', isLoggedIn, (req, res) => {
        // const [listRoom, listUser] = await Promise.all([

        // ])
        RoomService.searchRoom(req.query.q).then(listRoom => {
            UsersService.searchUser(req.query.q).then(listUser => {
                var data = {
                    listRoom: listRoom,
                    listUser: listUser
                }
                return res.status(200).json(data);
            })
        })


    });
};

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/account/login');
}