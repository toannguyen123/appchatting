var gulp = require('gulp');
var sass = require('gulp-sass'),
  watch = require('gulp-watch'),
  connect = require('gulp-connect'),
  cssnano = require('gulp-cssnano'),
  jshint = require('gulp-jshint')
uglify = require('gulp-uglify'),
  gulpIf = require('gulp-if'),
  imagemin = require('gulp-imagemin');


gulp.task('connectBuild', function () {
  connect.server({
    root: 'build',
    port: 8001,
    livereload: true
  });
});

gulp.task('css', function () {
  return gulp.src('./src/sass/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./build/css'))
    .pipe(connect.reload())
});

gulp.task('js', function () {
  return gulp.src('./src/js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(gulp.dest('./build/js'))
    .pipe(connect.reload())
  // .pipe( notify('JS task complete!') )
});

gulp.task('html', function () {
  return gulp.src('./src/*.html')
    .pipe(connect.reload())
    .pipe(gulp.dest('./build'))
});

gulp.task('images', function () {
  return gulp.src('./src/images/**/*.+(png|jpg|gif|svg)')
    .pipe(imagemin())
    .pipe(gulp.dest('./build/images'))
});

gulp.task('useref', function () {
  return gulp.src('./src/css/*.css')
    // Minifies only if it's a CSS file
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('./build/css'))
    .pipe(connect.reload())
});

/* Default task */
gulp.task('default', ['connectBuild', 'watch'], function () {
  gulp.start('css', 'js', 'html', 'useref', 'images');
});

/* Watch task */
gulp.task('watch', function () {
  gulp.watch('./src/css/*.css', ['css']);
  gulp.watch('./src/js/*.js', ['js']);
  gulp.watch('./src/*.html', ['html']);
});