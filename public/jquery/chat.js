$(function () {
    //make connection
    var socket = io.connect('http://'+$(location).attr('host'));
    //buttons and inputs
    var message = $("#comment");
    var username = $("#username");
    var send_message = $(".reply-send");
    var send_username = $("#send_username");
    var chatroom = $("#conversation");
    var feedback = $("#feedback");
    var friendUser = $("#idFriendUser");
    var currentUser = $("#idCurrentUser");
    var userNameCurrentUser = $('#userNameCurrentUser');
    var channel = $('#idChannel');
    var room = $('#idRoom');
    var searchText = $('#searchText');
    //Emit message
    send_message.click(function () {
        EmitSendMessage();
    })
    message.keypress(function (e) {
        if (e.which == 13) {
            EmitSendMessage();
        }
    });

    searchText.keypress(function (e) {
        if (e.which == 13) {
            SearchData();
        }
    });

    function SearchData() {
            $.ajax({
                url: '/search/searchUserOrMessage?q=' + searchText.val(),
                method: 'get',
                success: function (data) {
                    if (data && data.listRoom) {
                        $('#sideBarChannel').html('');
                        for (i = 0; i < data.listRoom.length; i++) {
                            let channel = `<div class="row sideBar-body" onClick="chatChannel('${data.listRoom[i].idroom}')">
                           <div class="col-sm-3 col-xs-3 sideBar-avatar">
                             <div class="avatar-icon">
                               <img src="https://bootdey.com/img/Content/avatar/avatar1.png">
                             </div>
                           </div>
                           <div class="col-sm-9 col-xs-9 sideBar-main">
                             <div class="row">
                               <div class="col-sm-8 col-xs-8 sideBar-name">
                                 <span class="name-meta">
                                 ${data.listRoom[i].title}
                                 </span>
                               </div>
                             </div>
                           </div>
                         </div>`;
                            $('#sideBarChannel').append(channel);
                        }
                    }
                    if (data && data.listRoom) {
                        $('#sideBarDirect').html('');
                        for (i = 0; i < data.listUser.length; i++) {
                            var status = "online";
                            if (!data.listUser[i].local.isonline) {
                                status = "offline";
                            }
                            let user = `<div class="row sideBar-body" onClick="chatDirect('${data.listUser[i]._id}')">
                           <div class="col-sm-3 col-xs-3 sideBar-avatar">
                             <div class="avatar-icon">
                               <img src="https://bootdey.com/img/Content/avatar/avatar1.png">
                             </div>
                           </div>
                           <div class="col-sm-9 col-xs-9 sideBar-main">
                             <div class="row">
                               <div class="col-sm-8 col-xs-8 sideBar-name">
                                 <span class="name-meta">
                                 ${data.listUser[i].local.username}
                                 </span>
                               </div>
                               <div class="col-sm-4 col-xs-4 pull-right sideBar-time">
                                 <span class="time-meta pull-right">
                                   ${status}
                                 </span>
                               </div>
                             </div>
                           </div>
                         </div>`;
                            $('#sideBarDirect').append(user);
                        }
                    }
                },
                error: function (ex) {
                    console.log('Searching error ' + ex)
                }
            })
    }

    function EmitSendMessage() {
        if (message.val()!==""&&message.val().trim()!=="") {
            socket.emit('new_message', {
                message: message.val(),
                idCurrentUser: currentUser.val(),
                userNameCurrentUser: userNameCurrentUser.val(),
                idFriendUser: friendUser.val(),
                idChannel: channel.val(),
                idRoom: room.val()
            });
        }
    }

    //listen on new_message
    socket.on("new_message", (data) => {
        if (data.idRoom == room.val()) {
            feedback.html('');
            message.val('');
            if (data.idCurrentUser == currentUser.val()) {
                let strchat = ` <div class="row message-body">
                <div class="col-sm-12 message-main-sender">
                  <div class="sender">
                    <div class="message-text">
                    ${ data.message}
                    </div>
                    <span class="message-time pull-right">
                    ${ data.userNameCurrentUser}
                    </span>
                  </div>
                </div>
              </div>`;

                chatroom.append(strchat)
            }
            else {
                let strchat = ` <div class="row message-body">
                <div class="col-sm-12 message-main-receiver">
                  <div class="receiver">
                    <div class="message-text">
                    ${ data.message}
                    </div>
                    <span class="message-time pull-right">
                    ${ data.userNameCurrentUser}
                    </span>
                  </div>
                </div>
              </div>`;
                chatroom.append(strchat)
                $('html, body').animate({
                    scrollTop: $("#conversation").offset().top
                }, 1500);
            }
        }
    })
    //Emit a username
    send_username.click(function () {
        socket.emit('change_username', { username: username.val() })
    })
    //Emit typing
    message.bind("keypress", () => {
        socket.emit('typing');

    })
    socket.on('typing', (data) => {
        feedback.html("<p><i>" + data.username + " is typing message... </i></p>")
    })
})
function chatDirect(id) {
    $.ajax({
        url: '/user/getUser?idFriendUser=' + id,
        method: 'get',
        success: function (data) {
            $('a.heading-name-meta').html(data.local.username);
            $('#idFriendUser').val(data._id);
            $('#idChannel').val('');
            $.ajax({
                url: '/user/getMessage?idFriendUser=' + id + '&idCurrentUser=' + $('#idCurrentUser').val(),
                method: 'get',
                success: function (data) {
                    var i;
                    var idCurrentUser = $('#idCurrentUser').val();
                    $("#conversation").html('');
                    $('#idRoom').val('');
                    if (data) {
                        $('#idRoom').val(data.idroom);
                        for (i = 0; i < data.messages.length; i++) {
                            var textMessage = '';
                            if (data.messages[i].iduser == idCurrentUser) {
                                textMessage = ` <div class="row message-body">
                                <div class="col-sm-12 message-main-sender">
                                  <div class="sender">
                                    <div class="message-text">
                                    ${ data.messages[i].text}
                                    </div>
                                    <span class="message-time pull-right">
                                    ${ data.messages[i].username}
                                    </span>
                                  </div>
                                </div>
                              </div>`;
                            }
                            else {
                                textMessage = ` <div class="row message-body">
                                <div class="col-sm-12 message-main-receiver">
                                  <div class="receiver">
                                    <div class="message-text">
                                    ${ data.messages[i].text}
                                    </div>
                                    <span class="message-time pull-right">
                                    ${ data.messages[i].username}
                                    </span>
                                  </div>
                                </div>
                              </div>`;
                            }

                            $("#conversation").append(textMessage)
                        }
                    }
                },
                error: function (ex) {
                    console.log("Error getUserById " + ex);
                }
            })
        },
        error: function (ex) {
            console.log("Error getUserById " + ex);
        }
    })
}