$(function () {
    var socket = io.connect('http://'+$(location).attr('host'));

    socket.on('reload_sidebar_channel', () => {
        ReloadSideBarChannel();
    });
    function ReloadSideBarChannel() {
        $.ajax({
            url: '/room/getAllRoom',
            method: 'get',
            success: function (data) {
                console.log(data);
                var i;
                $('#sideBarChannel').html('');
                for (i = 0; i < data.length; i++) {
                    var channel = `<div class="row sideBar-body" onClick="chatChannel('${data[i].idroom}')">
                   <div class="col-sm-3 col-xs-3 sideBar-avatar">
                     <div class="avatar-icon">
                       <img src="https://bootdey.com/img/Content/avatar/avatar1.png">
                     </div>
                   </div>
                   <div class="col-sm-9 col-xs-9 sideBar-main">
                     <div class="row">
                       <div class="col-sm-8 col-xs-8 sideBar-name">
                         <span class="name-meta">
                         ${data[i].title}
                         </span>
                       </div>
                     </div>
                   </div>
                 </div>`;
                    $('#sideBarChannel').append(channel);
                }
            },
            error: function (ex) {
                console.log("Error getUserById " + ex);
            }
        })
    }

    $('.btn-save-group').click(function () {
        var nameChannel = $('#nameGroupChat').val();
        if (nameChannel === "") {
            alert('Please input name channel');
        }
        $.ajax({
            url: '/room/addRoom?nameRoom=' + nameChannel,
            method: 'get',
            success: function (data) {
                chatChannel(data.idroom);
                $('#nameGroupChat').val('');
                $('#nameChannelModal').modal('toggle');
                socket.emit('reload_sidebar_channel', {});
                ReloadSideBarChannel();
            },
            error: function (ex) {
                console.log("Error getUserById " + ex);
            }
        })
    })

})
function chatChannel(id) {
    $('#idChannel').val(id);
    if (id) {
        $.ajax({
            url: '/room/getMessage?idChannel=' + id,
            method: 'get',
            success: function (data) {
                var i;
                var idCurrentUser = $('#idCurrentUser').val();
                $('a.heading-name-meta').html(data.title);
                $('#idRoom').val(data.idroom);
                console.log("message" + data);
                $("#conversation").html('');
                for (i = 0; i < data.messages.length; i++) {
                    var textMessage = '';
                    if (data.messages[i].iduser == idCurrentUser) {
                        textMessage = ` <div class="row message-body">
                                <div class="col-sm-12 message-main-sender">
                                  <div class="sender">
                                    <div class="message-text">
                                    ${ data.messages[i].text}
                                    </div>
                                    <span class="message-time pull-right">
                                    ${ data.messages[i].username}
                                    </span>
                                  </div>
                                </div>
                              </div>`;
                    }
                    else {
                        textMessage = ` <div class="row message-body">
                                <div class="col-sm-12 message-main-receiver">
                                  <div class="receiver">
                                    <div class="message-text">
                                    ${ data.messages[i].text}
                                    </div>
                                    <span class="message-time pull-right">
                                    ${ data.messages[i].username}
                                    </span>
                                  </div>
                                </div>
                              </div>`;
                    }

                    $("#conversation").append(textMessage)
                }

            },
            error: function (ex) {
                console.log("Error getUserById " + ex);
            }
        })
    }
    else {
        $("#conversation").html('');
    }


}

$(function () {
    chatChannel($('#idRoom').val());
})();