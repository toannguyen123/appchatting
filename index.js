const express = require('express');
const app = express();
const PORT = process.env.PORT || 5000;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var configDB = require('./config/database.js');
var RoomService = require('./app/services/roomservice');


mongoose.connect(configDB.url);
require('./config/passport')(passport); // 

// Cấu hình ứng dụng express
app.use(morgan('dev')); // sử dụng để log mọi request ra console
app.use(cookieParser()); // sử dụng để đọc thông tin từ cookie
app.use(bodyParser()); // lấy thông tin từ form HTML

app.use(session({ secret: 'xxxxxxxxxxxxx' }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./app/routers')(app, passport); // load các routes từ các nguồn


//Set the template engine ejs
app.set('view engine', 'ejs');
//middlewares
app.use(express.static('public'))
var clients = [];

server = app.listen(PORT)

//socket.io instantiation
const io = require("socket.io")(server)

io.sockets.on('connect', function (client) {
    clients.push(client);

    client.on('disconnect', function () {
        clients.splice(clients.indexOf(client), 1);
    });
});

//listen on every connection

io.on('connection', (socket) => {
    //listen on new_message
    socket.on('new_message', (data) => {
        io.sockets.emit('new_message', {
            message: data.message,
            idCurrentUser: data.idCurrentUser,
            idFriendUser: data.idFriendUser,
            userNameCurrentUser: data.userNameCurrentUser,
            idRoom:data.idRoom
        });
        let roomId = data.idCurrentUser + "-" + data.idFriendUser;
        let typeRoom="direct";
        if(data.idChannel){
            roomId=data.idChannel;
            typeRoom="channel";
        }
        let roomIdNew = data.idFriendUser + "-" + data.idCurrentUser;
        let idCurrentUser = data.idCurrentUser;
        let message = data.message;
        let userName= data.userNameCurrentUser;
        RoomService.findOneRoomById(roomId).then(values => {
            if (values == null) {
                if(data.idChannel){
                    roomIdNew=data.idChannel;
                }
                RoomService.findOneRoomById(roomIdNew).then(values => {
                    //Create new room 
                    if (values == null) {
                        let data = {
                            idroom: roomIdNew,
                            typeRoom:typeRoom,
                            messages: [{ iduser: idCurrentUser,username:userName, text: message }],
                        };
                        RoomService.createroom(data).then(values => {
                            return values;
                        })
                    }
                    //Push new  message with id room
                    else{
                        let data = {
                            idroom: roomIdNew,
                            messages: { iduser: idCurrentUser,username:userName, text: message },
                            date:Date.now 
                        };
                        const updatedRoom = RoomService.updateMessage(data);
                        updatedRoom.then(values => {
                            return values;
                        })
                    }
                });
            }
             //Push new  message with id room
            else {
                let data = {
                    idroom: roomId,
                    messages: { iduser: idCurrentUser,username:userName, text: message },
                    date:Date.now 
                };
                const updatedRoom = RoomService.updateMessage(data);
                updatedRoom.then(values => {
                    return values;
                })
            }

        });
    })
    socket.on('reload_sidebar_channel', (data) => {
        io.sockets.emit('reload_sidebar_channel', {
        })
    })
    //listen on typing
    socket.on('typing', (data) => {
        socket.broadcast.emit('typing', { username: socket.username })
    })
})